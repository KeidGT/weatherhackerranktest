create sequence seq_weather;
create sequence seq_temperature;


CREATE TABLE weather (
    id BIGINT default seq_weather.nextval PRIMARY KEY, 
    date TIMESTAMP,
    lat DOUBLE, 
    lon DOUBLE, 
    city VARCHAR(200),
    state VARCHAR(200)
);

CREATE TABLE temperature (
    id BIGINT  default seq_temperature.nextval PRIMARY KEY, 
    weather_id BIGINT NOT NULL, 
    value DOUBLE,
    constraint fk_weather foreign key (weather_id) references weather (id)
);

/*
insert into weather(date, lat, lon, city, state) values(CURRENT_TIMESTAMP(), 36.1189, -86.6892, 'Nashville', 'Tennessee');
insert into temperature(weather_id, value) values(1, 17.3);
insert into temperature(weather_id, value) values(1, 16.8);
insert into temperature(weather_id, value) values(1, 16.4);
insert into temperature(weather_id, value) values(1, 16.0);
insert into temperature(weather_id, value) values(1, 15.6);
insert into temperature(weather_id, value) values(1, 15.3);
insert into temperature(weather_id, value) values(1, 15.3);
insert into temperature(weather_id, value) values(1, 15.0);
insert into temperature(weather_id, value) values(1, 15.0);
insert into temperature(weather_id, value) values(1, 14.9);
insert into temperature(weather_id, value) values(1, 15.8);
insert into temperature(weather_id, value) values(1, 18.0);
insert into temperature(weather_id, value) values(1, 20.2);
insert into temperature(weather_id, value) values(1, 22.3);
insert into temperature(weather_id, value) values(1, 23.8);
insert into temperature(weather_id, value) values(1, 24.9);
insert into temperature(weather_id, value) values(1, 25.5);
insert into temperature(weather_id, value) values(1, 25.7);
insert into temperature(weather_id, value) values(1, 24.9);
insert into temperature(weather_id, value) values(1, 23.0);
insert into temperature(weather_id, value) values(1, 21.7);
insert into temperature(weather_id, value) values(1, 20.8);
insert into temperature(weather_id, value) values(1, 29.9);
insert into temperature(weather_id, value) values(1, 29.2);
insert into temperature(weather_id, value) values(1, 28.6);
insert into temperature(weather_id, value) values(1, 28.1);
*/