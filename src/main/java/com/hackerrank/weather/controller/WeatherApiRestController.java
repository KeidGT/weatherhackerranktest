package com.hackerrank.weather.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.hackerrank.weather.model.Weather;
import com.hackerrank.weather.service.WeatherService;
import com.hackerrank.weather.config.WeatherApi;
import com.hackerrank.weather.config.WeatherException;


@WeatherApi
public class WeatherApiRestController {

   
    @Autowired
    private WeatherService service;

    @RequestMapping(method = RequestMethod.GET, value = "/weather", produces = "application/json" )
    public Object getList() throws WeatherException {
        return this.service.getAll();
	}


    @RequestMapping(method = RequestMethod.GET, value = "/weather/{id}", produces = "application/json")
    public Object getSuspension(@PathVariable(value = "id", required = true) String id){
        return this.service.get(id);
    }


    @RequestMapping(method = RequestMethod.POST, value = "/weather", produces = "application/json" , consumes = "application/json")
    public Object solicitarReestablecer(@RequestBody final Weather request
        ) throws WeatherException {
		return this.service.save(request);
	}
    


}