package com.hackerrank.weather.config;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import com.hackerrank.weather.dto.ExceptionDto;
import com.hackerrank.weather.dto.ResponseDto;

/**
 * Response Modifier.
 */
@ControllerAdvice
public class ResponseBodyAdvisor implements ResponseBodyAdvice<Object> {

	/** Objeto Logger. */
	Logger logger = LoggerFactory.getLogger(ResponseBodyAdvisor.class);

	/**
	 * The custom response will be applied in this package
	 */
	protected String BASE_PACKAGE() {
		return "com.hackerrank.weather.controller";
	}

	/**
	 * Modify response body mesage.
	 *
	 * @param body                  Message body
	 * @param returnType            Obtect type that it returns
	 * @param selectedContentType   Obtect type that is selected
	 * @param selectedConverterType Object converter type
	 * @param request               Request object
	 * @param response              Response object
	 * @return Retorna respose modificado
	 */
	@Override
	public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType,
			Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request,
			ServerHttpResponse response) {
		// In the event that the output is already a ResponseDto object, that object
		// will be sent directly
		if (body instanceof ResponseDto) {
			if (response instanceof ServletServerHttpResponse) {
				response.setStatusCode(HttpStatus.valueOf(((ResponseDto) body).getCode()));
			}

			return body;
		}

		// Otherwise, it will pack the output into a ResponseDto
		ResponseDto data = new ResponseDto();
		int status = HttpStatus.OK.value();
		switch (request.getMethod().name().toString()) {
			case "GET":
				status = HttpStatus.OK.value();
				break;
			case "POST":
				status = HttpStatus.CREATED.value();
				break;

		}

		if (response instanceof ServletServerHttpResponse) {
			// If it is an ExceptionDto, there the status of the response will be the one
			// that brings the object
			if (body instanceof ExceptionDto) {
				// response.setStatusCode(((ExceptionDto) body).getStatus());
				status = ((ExceptionDto) body).getStatus().value();
				body = ((ExceptionDto) body).getMessage();
			}
			// status = ((ServletServerHttpResponse)
			// response).getServletResponse().getStatus();

			// If it is an error hashmap, its parameters will be verified to transform them
			// to the desired format
			if (body instanceof HashMap<?, ?>) {
				if (((HashMap<?, ?>) body).containsKey("error")) {
					Boolean isError = (Boolean) ((HashMap<?, ?>) body).get("error");

					if (isError) {
						if (((HashMap<?, ?>) body).containsKey("status")) {
							HttpStatus errorStatus = (HttpStatus) ((HashMap<?, ?>) body).get("status");
							status = errorStatus.value();
						} else {
							status = HttpStatus.BAD_REQUEST.value();
						}

						if (((HashMap<?, ?>) body).containsKey("message")) {
							String message = (String) ((HashMap<?, ?>) body).get("message");
							body = message;
						} else {
							body = "Ocurrio un error y el HashMap no fue correctamente formateado";
						}
					}
				}
			}

		}

		data.setCode(status);
		response.setStatusCode(HttpStatus.valueOf(status));
		data.setMessage(HttpStatus.valueOf(status).name().toString());
		data.setResponse(body);
		return data;
	}

	/**
	 * Checks the type of object that will be modified in its response.
	 * Response is universal to the project so leave the value true.
	 * 
	 * @param returnType    Type of object that returns
	 * @param converterType Returning Object Convert Type
	 * @return true, True by default
	 */
	@Override
	public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
		this.logger.info(" ResponseBodyAdvice aplicado a  " + returnType.getContainingClass().getPackage().getName());
		return returnType.getContainingClass().getPackage().getName().equals(this.BASE_PACKAGE());
	}

	/**
	 * Exception Handler that helps exceptions from the original service to be
	 * displayed unchanged in this service
	 */
	@ExceptionHandler(GatewayException.class)
	public ResponseEntity<ResponseDto> handleException(GatewayException ex) {
		return new ResponseEntity<ResponseDto>(ex.getResponse(), HttpStatus.valueOf(ex.getResponse().getCode()));
	}

	@ExceptionHandler(WeatherException.class)
	public ResponseEntity<ResponseDto> exceptionInterceptor(
			WeatherException ex) {
		return new ResponseEntity<ResponseDto>(
				new ResponseDto(
						ex.getStatus().value(),
						ex.getStatus().name(),
						ex.getMessage()),
				ex.getStatus());
	}

}