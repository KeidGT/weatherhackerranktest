package com.hackerrank.weather.config;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.annotation.ElementType;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/weather/v1/api")
@CrossOrigin(origins = "*")
@EnableCaching
@EntityScan(basePackages = "com.hackerrank.weather.model")
@ComponentScan({"com.hackerrank.weather.repository"})
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface WeatherApi {
    
}
