package com.hackerrank.weather.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hackerrank.weather.dto.ResponseDto;


public class GatewayException extends Exception {
	
	Logger logger = LoggerFactory.getLogger(GatewayException.class);

	private static final long serialVersionUID = 6604392639368578575L;
	
	private ResponseDto response;

	public GatewayException(ResponseDto response) {
		super(response.getResponse().toString());
		logger.warn(response.getResponse().toString());
		this.response = response;
	}	
	
	public GatewayException(String message) {
		super(message);
		logger.warn(message);
	}	

	public GatewayException(String message, Throwable cause) {
		super(message, cause);
	}

	public ResponseDto getResponse() {
		return response;
	}

	public void setResponse(ResponseDto response) {
		this.response = response;
	}
	
}
