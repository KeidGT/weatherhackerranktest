package com.hackerrank.weather.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;


public class WeatherException extends Exception {
	
	Logger logger = LoggerFactory.getLogger(WeatherException.class);

	private static final long serialVersionUID = 6604392639368578575L;
	
	/** Message Status. (Default BAD_REQUEST) */
	private HttpStatus status;

	public WeatherException(String message) {
		super(message);
		logger.warn(message);
		status = HttpStatus.BAD_REQUEST;
	}	

	public WeatherException(String message, Throwable cause) {
		super(message, cause);
		status = HttpStatus.BAD_REQUEST;
	}
	
	public WeatherException(String message, HttpStatus status) {
		super(message);
		this.status = status;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}
}
