package com.hackerrank.weather.service;

import java.util.List;

import com.hackerrank.weather.model.Weather;

public interface WeatherService extends Service<Weather> {
    public List<Weather> getByAllAndLimit(Integer size, String search);
}
