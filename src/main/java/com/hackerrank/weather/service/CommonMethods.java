package com.hackerrank.weather.service;

import com.hackerrank.weather.dto.ResponseDto;

public abstract class CommonMethods {
    
    protected ResponseDto getBaseResponse() {
        ResponseDto response = new ResponseDto();
        response.setMessage("succcess");
        response.setCode(200);
        return response;
    }
}
