package com.hackerrank.weather.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hackerrank.weather.model.Temperatures;
import com.hackerrank.weather.model.Weather;
import com.hackerrank.weather.repository.TemperaturesRepository;
import com.hackerrank.weather.repository.WeatherRepository;

@Service
public class WeatherServiceImpl extends CommonMethods implements WeatherService{


    @Autowired
    private WeatherRepository repository;

    @Autowired
    private TemperaturesRepository repositoryTemperatures;

    @Override
    public Weather get(Object id) {
        Optional<Weather> optWeather = repository.findById(Integer.parseInt(String.valueOf(id)));
        if(optWeather.isPresent()){
             return optWeather.get();
        }
        return new Weather();
    }

    @Override
    public List<Weather> getAll() {
        List<Weather> listWeather = repository.findAll();
        if(listWeather!=null){
             return listWeather;
        }
        return new ArrayList<>();
    }

    @Override
    public Weather save(Weather t) {
        // TODO Auto-generated method stub
        repository.save(t);
        for(Double temp : t.getTemperatures()){
            Temperatures temperature = new Temperatures();
            temperature.setWeather(t);
            temperature.setValue(temp);
            repositoryTemperatures.save(temperature);

        }
        return t;
    }

    @Override
    public Weather update(Weather t) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Weather delete(Weather t) {
        // TODO Auto-generated method stub
        return null;
    }


    @Override
    public List<Weather> getByAllAndLimit(Integer size, String search){

        return null;
    }
    
    
}
