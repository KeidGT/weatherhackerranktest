package com.hackerrank.weather.service;


public interface Service<T> {
    
    Object get(Object id);

    Object getAll();
    
    Object save(T t);

    Object update(T t);

    Object delete(T t);

}
