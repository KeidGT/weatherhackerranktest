package com.hackerrank.weather.dto;

import lombok.Data;

@Data
public class ResponseDto {
    
    public ResponseDto(){
        
    }
    public ResponseDto(int code, String response, String message) {
        this.code = code;
        this.response = response;
        this.message = message;
    }
    /* HTTP Response code*/
    private Integer code;
    /* Response message*/
    private String message;
    /* Response */
    private Object response;

}
