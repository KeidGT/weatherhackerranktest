package com.hackerrank.weather.dto;

import java.io.Serializable;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;


/**
 * DTO Class for error management.
 *
 */
@Data
public class ExceptionDto implements Serializable {

	/** Message serial UID. */
	private static final long serialVersionUID = -6960961936267655712L;

	/**
	 * Default Constructor, default response is BAD_REQUEST
	 *
	 * @param message Exception Message
	 */
	public ExceptionDto(String message) {
		this.message = message;
		this.status = HttpStatus.BAD_REQUEST;
	}
	
	/**
	 * Custom message exception constructor.
	 *
	 * @param message Exception Message
	 * @param status HttpStatus Response
	 */
	public ExceptionDto(String message, HttpStatus status) {
		this.message = message;
		this.status = status;
	}

	/** Informacion del mensaje. */
	private String message;
	
	/** Status del mensaje. (por defecto sera BAD_REQUEST) */
	@JsonIgnore
	private HttpStatus status;

}
