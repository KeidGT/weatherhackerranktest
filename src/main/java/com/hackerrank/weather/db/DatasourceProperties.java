package com.hackerrank.weather.db;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "spring.datasource")
@Data
public class DatasourceProperties {

	String url;
    String driverClassName;	
    String username;
    String password;	

}
