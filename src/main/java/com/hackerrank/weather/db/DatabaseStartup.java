package com.hackerrank.weather.db;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;



import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;


@Component
public class DatabaseStartup implements ApplicationListener<ApplicationReadyEvent> {



    @Autowired
    DatasourceProperties dataSourceProperties;
    
    /**
     * This event is executed as late as conceivably possible to indicate that
     * the application is ready to service requests.
     */
    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {

        createDatabase();

        return;
    }

    
    private boolean createDatabase() {
        Connection conn = null;
        Statement stmt = null;

        boolean response  = false;
        try {
            InputStream temp = DatabaseStartup.class.getResourceAsStream("/sql/database.sql");
            String sql = IOUtils.toString(temp, StandardCharsets.UTF_8);

            Class.forName("org.h2.Driver");
            conn = DriverManager.getConnection(dataSourceProperties.getUrl(), dataSourceProperties.getUsername(), dataSourceProperties.getPassword());

            stmt = conn.createStatement();

            int result = stmt.executeUpdate(sql);
            System.out.println("DatabaseCreationResult:" + result);
            response = (result != -1);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return response;
    }

}