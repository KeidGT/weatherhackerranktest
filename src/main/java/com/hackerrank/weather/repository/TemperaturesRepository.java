package com.hackerrank.weather.repository;

import com.hackerrank.weather.model.Temperatures;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TemperaturesRepository extends JpaRepository<Temperatures, Integer> {
}
