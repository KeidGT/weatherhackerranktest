package com.hackerrank.weather.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Table(name="temperature")
public class Temperatures {

    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "st")  
    @SequenceGenerator(name="st", sequenceName = "seq_weather", allocationSize=1)
    private Integer id;

    @Column(name = "value")
    private Double value;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "weather_id", referencedColumnName = "id")
    private Weather weather;

    public Temperatures() {
    }
}
