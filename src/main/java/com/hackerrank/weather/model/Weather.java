package com.hackerrank.weather.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Table(name = "weather")
public class Weather implements Serializable {

    private static final long serialVersionUID = 45656854867956L;

    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "sw")  
    @SequenceGenerator(name="sw", sequenceName = "seq_weather", allocationSize=1)
    private Integer id;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @Column(name = "date", columnDefinition = "TIMESTAMP")
    private Date date;

    @Column(name = "lat")
    private Float lat;

    @Column(name = "lon")
    private Float lon;

    @Size(max = 200)
    @Column(name = "city")
    private String city;

    @Size(max = 200)
    @Column(name = "state")
    private String state;

    @JsonIgnore
    @OneToMany(mappedBy = "weather", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Temperatures> temperaturesList;

    @Transient
    private List<Double> temperatures;

    public List<Double> getTemperatures() {
        setTemperatures();
        return this.temperatures;
    }

    public Weather() {
    }

    public Weather(Date date, Float lat, Float lon, String city, String state, List<Double> temperatures) {
        this.date = date;
        this.lat = lat;
        this.lon = lon;
        this.state = state;
        this.temperatures = temperatures;
    }

    public Weather(Integer id, Date date, Float lat, Float lon, String city, String state, List<Double> temperatures) {
        this.id = id;
        this.date = date;
        this.lat = lat;
        this.lon = lon;
        this.state = state;
        this.temperatures = temperatures;
    }

    private void setTemperatures(){
        if(this.temperatures==null && this.temperaturesList!=null || (this.temperatures!=null && this.temperaturesList!=null && this.temperatures.size()!= this.temperaturesList.size())){
            List<Double> temp = new ArrayList<>();
        for (Temperatures t : temperaturesList) {
            temp.add(t.getValue());
        }
        this.temperatures = temp;
        }

    }
}
